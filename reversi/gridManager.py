import pygame, sys, re, setting
from queue import Queue
from pygame.locals import *
from enum import Enum, unique, Flag

class Grid():
    def __init__(self):
        self.surfaceEmpty = pygame.image.load("Sprites/Empty.png")
        self.surfaceX = pygame.image.load("Sprites/X.png")
        self.surfaceO = pygame.image.load("Sprites/O.png")

        self.arr = [[0,0,0,0,0,0,0,0],
                    [0,0,0,0,0,0,0,0],
                    [0,0,0,0,0,0,0,0],
                    [0,0,0,1,-1,0,0,0],
                    [0,0,0,-1,1,0,0,0],
                    [0,0,0,0,0,0,0,0],
                    [0,0,0,0,0,0,0,0],
                    [0,0,0,0,0,0,0,0]]

    def draw(self):
        for i in range(0,8):
            for j in range(0,8):
                if self.arr[i][j] == 0:
                    setting.DISPLAYSURF.blit(self.surfaceEmpty, (setting.INIT_X + j*setting.STEP, setting.INIT_Y + i*setting.STEP))
                elif self.arr[i][j] == 1:
                    setting.DISPLAYSURF.blit(self.surfaceX, (setting.INIT_X + j*setting.STEP, setting.INIT_Y + i*setting.STEP))
                elif self.arr[i][j] == -1:
                    setting.DISPLAYSURF.blit(self.surfaceO, (setting.INIT_X + j*setting.STEP, setting.INIT_Y + i*setting.STEP))

    def valid_move(self,x,y,value):
        # Kiểm tra xem ô (y, x) trên bàn cờ có trống không
        if self.arr[y][x] != 0:
            return False
    
        for i in range(-1, 2):
            for j in range(-1, 2):
                if i == 0 and j == 0:
                    continue
                # Kiểm tra các ô bên cạnh ô (y, x)
                row, col = y + i, x + j
                found_opponent = False
                while 0 <= row < 8 and 0 <= col < 8 and self.arr[row][col] == -value:
                    row, col = row + i, col + j
                    found_opponent = True
                if found_opponent and 0 <= row < 8 and 0 <= col < 8 and self.arr[row][col] == value:
                    return True
    
        return False

    def make_move(self, x, y, value):
        if not self.valid_move(x, y, value):
            return False
    
        self.arr[y][x] = value
        for i in range(-1, 2):
            for j in range(-1, 2):
                if i == 0 and j == 0:
                    continue
                row, col = y + i, x + j
                found_opponent = False
                while 0 <= row < 8 and 0 <= col < 8 and self.arr[row][col] == -value:
                    row, col = row + i, col + j
                    found_opponent = True
                if found_opponent and 0 <= row < 8 and 0 <= col < 8 and self.arr[row][col] == value:
                    row, col = y + i, x + j
                    while self.arr[row][col] == -value:
                        self.arr[row][col] = value
                        row, col = row + i, col + j
        return True

    def numOfValue(self, value):
        count = 0
        for i in range(0,8):
            for j in range(0,8):
                if self.arr[i][j] == value:
                    count += 1
        return count

grid = Grid()
initalGrid = Grid()

