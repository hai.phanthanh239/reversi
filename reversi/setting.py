import pygame, sys, re, time
from queue import Queue
from pygame.locals import *
from enum import Enum, unique, Flag

class STATUS(Enum) :
    HORIZONTAL = 0
    VERTICAL = 1
    STAND = 2
    NOT_VALID = 3


BLACK = (  0,   0,   0)
WHITE = (255, 255, 255)
RED   = (255,   0,   0)
GREEN = (  0, 255,   0)
BLUE  = (  0,   0, 255)

STEP = 50 #Khoảng cách giữa mỗi bước đi
SPEED = 5 #STEP phai chia het cho SPEED

INIT_X = 100 #Phải chia hết cho STEP
INIT_Y = 100 #Phải chia hết cho STEP

WINDOWWIDTH = 600 # Chiều dài cửa sổ
WINDOWHEIGHT = 600 # Chiều cao cửa sổ

DISPLAYSURF = pygame.display.set_mode((WINDOWWIDTH, WINDOWHEIGHT))
pygame.display.set_caption('REVERSI')
