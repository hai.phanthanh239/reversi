def get_valid_moves(cur_state,turn):
    Available_moves = []
    for i in range(8):
        for j in range(8):
            if cur_state[i][j] == 0:
                directs = [(-1,-1),(0,-1),(1,-1),(-1,0),(1,0),(-1,1),(0,1),(1,1)]
                flip_points = set()
                for di,dj in directs:
                    k,l = i+di,j+dj
                    temp_points = set()
                    while 0<=k<8 and 0<=l<8 and cur_state[k][l]==-turn:
                        temp_points.add((k,l))
                        k+=di
                        l+=dj
                    if 0<=k<8 and 0<=l<8 and cur_state[k][l] == turn:
                        flip_points |= temp_points
                
                if flip_points:
                    Available_moves.append((i,j,flip_points))
        
    return Available_moves
                        
    

def make_move(cur_state,turn,move):
    i,j,flip_points = move
    new_state = [p[:] for p in cur_state]
    for f_i,f_j in flip_points:
        new_state[f_i][f_j] = -new_state[f_i][f_j]
    new_state[i][j] = turn
    return new_state
    

def evaluate(state):
    X_score = sum([row.count(1) for row in state])
    O_score = sum([row.count(-1) for row in state])
    return X_score - O_score + 100*(state[0][7] + state[0][0]+ state[7][0]+ state[7][7])


def minimax(state, depth, turn):
    if depth == 0 or len(get_valid_moves(state, turn)) == 0:
        return None,evaluate(state)

    best_move = None
    if turn == 1:
        best_value = float('-inf')
        for move in get_valid_moves(state, turn):
            new_state = make_move(state, turn, move)
            _,value = minimax(new_state, depth - 1, -turn)

            if best_value < value:
                best_value = value
                best_move = move
        return best_move,best_value
    else:
        best_value = float('inf')
        for move in get_valid_moves(state, turn):
            new_state = make_move(state, turn, move)
            _,value = minimax(new_state, depth - 1, -turn)
            if best_value > value:
                best_value = value
                best_move = move
        return best_move,best_value

def select_move(cur_state, player_to_move, remain_time):

    if remain_time > 15:
        best_move,value = minimax(cur_state,4,player_to_move)
    elif remain_time > 7:
        best_move,value = minimax(cur_state,3,player_to_move)
    elif remain_time > 3:
        best_move,value = minimax(cur_state,2,player_to_move)
    else: 
        best_move,value = minimax(cur_state,1,player_to_move)

    if best_move:
        return (best_move[1],best_move[0])
    else:
        return None
