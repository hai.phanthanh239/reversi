﻿def get_valid_moves(cur_state,turn):
    Available_moves = []
    for i in range(8):
        for j in range(8):
            if cur_state[i][j] == 0:
                directs = [(-1,-1),(0,-1),(1,-1),(-1,0),(1,0),(-1,1),(0,1),(1,1)]
                flip_points = set()
                for di,dj in directs:
                    k,l = i+di,j+dj
                    temp_points = set()
                    while 0<=k<8 and 0<=l<8 and cur_state[k][l]==-turn:
                        temp_points.add((k,l))
                        k+=di
                        l+=dj
                    if 0<=k<8 and 0<=l<8 and cur_state[k][l] == turn:
                        flip_points |= temp_points
                
                if flip_points:
                    Available_moves.append((i,j,flip_points))
        
    return Available_moves
                        
    

def make_move(cur_state,turn,move):
    i,j,flip_points = move
    new_state = [p[:] for p in cur_state]
    for f_i,f_j in flip_points:
        new_state[f_i][f_j] = -new_state[f_i][f_j]
    new_state[i][j] = turn
    return new_state
    
#------------All Evaluate functions ----------------#

WEIGHTS = [100, -3, 10, 10, 10, 10, -3, 100,
               -3, -4, -1, -1, -1, -1, -4, -3,
               10, -1, 1, 0, 0, 1, -1, 10,
               10, -1, 0, 1, 1, 0, -1, 10,
               10, -1, 0, 1, 1, 0, -1, 10,
               10, -1, 1, 0, 0, 1, -1, 10,
               -3, -4, -1, -1, -1, -1, -4, -3,
               100, -3, 10, 10, 10, 10, -3, 100]

#-- focus on important point
def get_weight(state, turn):
    total = 0
    i = 0
    while i < 64:
        #caculate point gain
        if state[int(i/8)][i%8] == turn:
            total += WEIGHTS[i]
        #caculate point lost
        if state[int(i/8)][i%8] == -turn:
            total -= WEIGHTS[i]
        i += 1
    #print('w ' + str(total))
    return total

#-- check in case lost to much point for a important point
def get_cost(state,turn):
    opponent_piece = sum([row.count(-turn) for row in state])
    self_piece = sum([row.count(turn) for row in state])
    #print('c ' + str(self_piece-opponent_piece))
    return self_piece-opponent_piece

#-- check how many stable pos of turn
def get_stable(state, turn):
    stable = set()
    for i in range(8):
        for j in range(8):
            if state[i][j] == turn:
                visited = set()
                stack = [(i, j)]
                is_stable = True
                while stack:
                    x, y = stack.pop()
                    visited.add((x, y))
                    # Kiểm tra xem ô hiện tại có nằm trên biên của bàn cờ hay không
                    if x == 0 or x == 7 or y == 0 or y == 7:
                        is_stable = False
                        break
                    # Kiểm tra xem ô hiện tại có bị đối thủ đánh lật được không
                    if (x-1, y) not in visited and state[x-1][y] == -turn:
                        stack.append((x-1, y))
                    if (x+1, y) not in visited and state[x+1][y] == -turn:
                        stack.append((x+1, y))
                    if (x, y-1) not in visited and state[x][y-1] == -turn:
                        stack.append((x, y-1))
                    if (x, y+1) not in visited and state[x][y+1] == -turn:
                        stack.append((x, y+1))
                if is_stable:
                    stable |= visited
    #print('s ' + str(len(stable)))
    return len(stable)

#-- main evaluate function -- alpha beta call this function
def evaluate(state, turn):
    return  2* get_weight(state, turn) + 3* get_cost(state, turn) + 2*get_stable(state, turn);

#-- main evaluate function for simple minimax function 
def old_evaluate(state):
    X_score = sum([row.count(1) for row in state])
    O_score = sum([row.count(-1) for row in state])
    return X_score - O_score + 100*(state[0][7] + state[0][0]+ state[7][0]+ state[7][7])
#---------------------------------------------------#

#------------Algorithm functions -------------------#
#-- minimax function -> indepent from alphabeta -> only use to test
def minimax(state, depth, turn):
    if depth == 0 or len(get_valid_moves(state, turn)) == 0:
        return None,old_evaluate(state)

    best_move = None
    if turn == 1:
        best_value = float('-inf')
        for move in get_valid_moves(state, turn):
            new_state = make_move(state, turn, move)
            _,value = minimax(new_state, depth - 1, -turn)

            if best_value < value:
                best_value = value
                best_move = move
        return best_move,best_value
    else:
        best_value = float('inf')
        for move in get_valid_moves(state, turn):
            new_state = make_move(state, turn, move)
            _,value = minimax(new_state, depth - 1, -turn)
            if best_value > value:
                best_value = value
                best_move = move
        return best_move,best_value

#-- alpha beta function ->handle alpha beta loop, return evaluate score
def alpha_beta(state, depth, turn, alpha, beta, ai_turn):
    # get valid move
    if depth == 0 or len(get_valid_moves(state, turn)) == 0:
        return evaluate(state, ai_turn)
    # get max value
    if turn == ai_turn:
        best_value = float('-inf')
        for move in get_valid_moves(state, turn):
            # make move -> get new state
            new_state = make_move(state, turn, move)
            value = alpha_beta(new_state, depth - 1, -turn, alpha, beta,ai_turn)
            if value > best_value:
                best_value = value
            #cut off
            if best_value >= beta:
                return best_value
            alpha = max (alpha,best_value)
        return alpha
    # get min value
    else:
        best_value = float('inf')
        for move in get_valid_moves(state, turn):
            new_state = make_move(state, turn, move)
            value = alpha_beta(new_state, depth - 1, -turn, alpha, beta, ai_turn)
            if value < best_value:
                best_value = value
            # cut off
            if best_value <= alpha:
                return best_value
            beta = min(beta,value)
        return beta

#-- alpha beta trigger 
# -> AI turn in reversi.py will call this function
# -> return best move
def alpha_beta_trigger(state, depth, turn, alpha, beta):
    if depth == 0 or len(get_valid_moves(state, turn)) == 0:
        return None
    best_move = None
    best_val = float('-inf')
    # get Max 
    for move in get_valid_moves(state, turn):
        new_state = make_move(state, turn, move)
        value = alpha_beta(new_state, depth-1, -turn, alpha, beta, turn)
        if best_val<value:
            best_move=move
            best_val=value
    return best_move
#---------------------------------------------------#

def print_state(state):
    for i in range(8):
        for j in range(8):
            if state[i][j] == 0:
                print('_ ',end='')
            elif state[i][j] == 1:
                print('X ',end='')
            else: 
                print('O ',end='')
        print('\n')

    print('\n')

