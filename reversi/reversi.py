import pygame
import sys
import re
import time
import os
import gridManager, ReversiSimulateMove
import setting
import os.path
import random
from queue import Queue
from pygame.locals import *
from enum import Enum, unique, Flag

pygame.init()

### Xác định FPS ###
FPS = 60
fpsClock = pygame.time.Clock()


font = pygame.font.SysFont('consolas', 18)

# ---------------------------------------function----------------------

def displayText():
    global xTurn, isEndGame, ai_turn, current_step_AI_time, total_AI_time, human_play
    if xTurn:
        textSurface = font.render('X TURN!', True, setting.RED)
    else:
        textSurface = font.render('O TURN!', True, setting.GREEN)
    setting.DISPLAYSURF.blit(textSurface, (100, 80))

    if ai_turn:
        textSurface = font.render('AI TURN!', True, setting.WHITE)
    else:
        if human_play == True:
            textSurface = font.render('HUMAN TURN!', True, setting.WHITE)
        else:
            textSurface = font.render('CPU TURN!', True, setting.WHITE)
    setting.DISPLAYSURF.blit(textSurface, (100, 60))

    textSurface = font.render('Current step AI time: ' + str(round(current_step_AI_time,4)) + 's', True, setting.WHITE)
    setting.DISPLAYSURF.blit(textSurface, (300, 60))
    textSurface = font.render('total AI time: ' + str(round(total_AI_time,4)) + 's', True, setting.WHITE)
    setting.DISPLAYSURF.blit(textSurface, (300, 80))

    textSurface = font.render('Num of X = ' + str(gridManager.grid.numOfValue(1)), True, setting.RED)
    setting.DISPLAYSURF.blit(textSurface, (0, 0))
    textSurface = font.render('Num of O = ' + str(gridManager.grid.numOfValue(-1)), True, setting.GREEN)
    setting.DISPLAYSURF.blit(textSurface, (0, 20))
    
    if isEndGame:
        textSurface = font.render('END GAME!', True, setting.WHITE)
        setting.DISPLAYSURF.blit(textSurface, (100, 40))

def getInputEvent():
    global xTurn, ai_turn
    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            sys.exit()
        #if event.type == pygame.MOUSEBUTTONDOWN:
        #    mouse = pygame.mouse.get_pos()
        #    x = int((mouse[0] - setting.INIT_X)/setting.STEP)
        #    y = int((mouse[1] - setting.INIT_Y)/setting.STEP)
        #    if 0<=x<8 and 0<=y<8:
        #        if ai_turn == False:
        #            if gridManager.grid.make_move(x,y,-ai_value):
        #                if ai_move() != None:
        #                    xTurn = not xTurn
        #                    ai_turn = True
        #                setting.DISPLAYSURF.fill(setting.BLACK)
        #                displayText()
        #                gridManager.grid.draw()

        #                pygame.display.update()
        #                fpsClock.tick(FPS)

def select_move(cur_state, player_to_move, remain_time):
    if remain_time > 15:
        best_move,value = ReversiSimulateMove.minimax(cur_state,4,player_to_move)
    elif remain_time > 7:
        best_move,value = ReversiSimulateMove.minimax(cur_state,3,player_to_move)
    elif remain_time > 3:
        best_move,value = ReversiSimulateMove.minimax(cur_state,2,player_to_move)
    else: 
        best_move,value = ReversiSimulateMove.minimax(cur_state,1,player_to_move)
    if best_move:
        return (best_move[1],best_move[0])
    else:
        return None

#def check_player_move():
#    for x in range(0,8):
#        for y in range(0,8):
#            if gridManager.grid.valid_move(x,y,-ai_value):
#                return True
#    return False
                
def ai_move():
    global total_AI_time, current_step_AI_time, ai_turn

    start_time = time.time()
    remain_time = 60-total_AI_time
    #-------------alpha_beta---------------#
    if remain_time > 15:
        print("Bac 4");
        best_move= ReversiSimulateMove.alpha_beta_trigger(gridManager.grid.arr,5,ai_value, float('-inf'),float('inf'))
    elif remain_time > 7:
        print("Bac 3");
        best_move = ReversiSimulateMove.alpha_beta_trigger(gridManager.grid.arr,5,ai_value, float('-inf'),float('inf'))
    elif remain_time > 3:
        print("Bac 2");
        best_move  = ReversiSimulateMove.alpha_beta_trigger(gridManager.grid.arr,2,ai_value, float('-inf'),float('inf'))
    else: 
        print("Bac 1");
        best_move = ReversiSimulateMove.alpha_beta_trigger(gridManager.grid.arr,1,ai_value, float('-inf'),float('inf'))
    #--------------------------------------#
    #-----------Minimax--------------------#
    # if remain_time > 15:
    #     print("Bac 4");
    #     best_move, value= ReversiSimulateMove.minimax(gridManager.grid.arr,4,ai_value)
    # elif remain_time > 7:
    #     print("Bac 3");
    #     best_move, value = ReversiSimulateMove.minimax(gridManager.grid.arr,3,ai_value)
    # elif remain_time > 3:
    #     print("Bac 2");
    #     best_move, value  = ReversiSimulateMove.minimax(gridManager.grid.arr,2,ai_value)
    # else: 
    #     print("Bac 1");
    #     best_move, value = ReversiSimulateMove.minimax(gridManager.grid.arr,1,ai_value)
    #--------------------------------------#

    if ai_turn:
        end_time = time.time()
        current_step_AI_time = end_time - start_time
        total_AI_time += current_step_AI_time

    if best_move:
        return best_move[1],best_move[0]
    else:
        return None

def cpu_move():
    #----------Random move-----------------#
    move_list = []
    for x in range(0,8):
        for y in range(0,8):
            if gridManager.grid.valid_move(x,y,-ai_value):
                move_list.append((x,y))

    if len(move_list) == 0:
        return None
    rand = random.randint(0,len(move_list)-1)
    return move_list[rand][0], move_list[rand][1]
    #--------------------------------------#

    #------minimax move (if want to compare)-------------#
    # total_CPU_time=0
    # current_step_CPU_time=0
    # start_time = time.time()

    # remain_time = 60-total_CPU_time

    # if remain_time > 15:
    #     print("Bac 4");
    #     best_move,value = ReversiSimulateMove.minimax(gridManager.grid.arr,4,ai_value)
    # elif remain_time > 7:
    #     print("Bac 3");
    #     best_move,value = ReversiSimulateMove.minimax(gridManager.grid.arr,3,ai_value)
    # elif remain_time > 3:
    #     print("Bac 2");
    #     best_move,value = ReversiSimulateMove.minimax(gridManager.grid.arr,2,ai_value)
    # else: 
    #     print("Bac 1");
    #     best_move,value = ReversiSimulateMove.minimax(gridManager.grid.arr,1,ai_value)

    # end_time = time.time()
    # current_step_CPU_time = end_time - start_time
    # total_CPU_time += current_step_CPU_time
    # print('distance',(total_AI_time-total_CPU_time))

    # if best_move:
    #     return best_move[1],best_move[0]
    # else:
    #     return None
    #--------------------------------------#
    

def human_valid_move():
    move_list = []
    for x in range(0,8):
        for y in range(0,8):
            if gridManager.grid.valid_move(x,y,-ai_value):
                return True
    return False
def ai_valid_move():
    move_list = []
    for x in range(0,8):
        for y in range(0,8):
            if gridManager.grid.valid_move(x,y,ai_value):
                return True
    return False
# -------------------------------------__MAIN___-----------------------------------

ai_value = -1 #1 la X, -1 la O
xTurn = True

human_play = True # Chon True neu la human danh voi AI, False neu CPU danh voi AI !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! (CPU la danh random)

total_AI_time = 0
current_step_AI_time = 0;

if (ai_value == 1 and xTurn == True) or (ai_value == -1 and xTurn == False):
    ai_turn = True
else:
    ai_turn = False

isEndGame = False

setting.DISPLAYSURF.fill(setting.BLACK)
gridManager.grid.draw()
displayText()
pygame.display.update()
fpsClock.tick(FPS)

while True:
    getInputEvent()

    if ai_turn == True:
        if ai_move() != None:
            ai_x, ai_y = ai_move()
            #print(ai_x,ai_y)
            #ReversiSimulateMove.print_state(gridManager.grid.arr)
            gridManager.grid.make_move(ai_x,ai_y,ai_value)

        xTurn = not xTurn
        ai_turn = False

        setting.DISPLAYSURF.fill(setting.BLACK)
        displayText()
        gridManager.grid.draw()
        #time.sleep(3)
    else:
        if human_play == False:
            if cpu_move()!= None:
                cpu_x, cpu_y = cpu_move()
                #print(cpu_x,cpu_y)
                #ReversiSimulateMove.print_state(gridManager.grid.arr)
                gridManager.grid.make_move(cpu_x,cpu_y,-ai_value)
        else:
            if human_valid_move()!=False:
                human_make_move = False;
                while human_make_move == False:
                    for event in pygame.event.get():
                        if event.type == QUIT:
                            pygame.quit()
                            sys.exit()
                        if event.type == pygame.MOUSEBUTTONDOWN:
                            mouse = pygame.mouse.get_pos()
                            x = int((mouse[0] - setting.INIT_X)/setting.STEP)
                            y = int((mouse[1] - setting.INIT_Y)/setting.STEP)
                            if 0<=x<8 and 0<=y<8:
                                gridManager.grid.make_move(x,y,-ai_value)
                                human_make_move = True
        xTurn=not xTurn
        ai_turn = True
        setting.DISPLAYSURF.fill(setting.BLACK)
        displayText()
        gridManager.grid.draw()
        if human_play == False:
            time.sleep(0.5)

    if ai_valid_move() == None and cpu_move() == None:
        isEndGame = True
        setting.DISPLAYSURF.fill(setting.BLACK)
        displayText()
        gridManager.grid.draw()

    pygame.display.update()
    fpsClock.tick(FPS)
s